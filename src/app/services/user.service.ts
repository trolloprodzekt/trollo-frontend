import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import {Observable} from "rxjs";
@Injectable({
  providedIn: 'root'
})
export class UserService {


  constructor(private http: HttpClient) { }

  register(user: User): Observable<any> {
       return this.http.post<any>(`http://localhost:4000/users/register`, user);
  }

  getAllUsers() {
    return this.http.get<User[]>(`http://localhost:4000/users/getAll`);
  }

}
