import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {TokenStorage} from "./token-storage.service";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient, private token: TokenStorage) {}

  public isAuthenticated(): boolean {
    const token = this.token.getToken();
    return token != null;
  }

  loginClient(username: string, password: string) {
    return this.http.post<any>( 'http://localhost:4000/users/authenticate', { username, password });
  }

  logout() {
    this.token.signOut();
  }

}
