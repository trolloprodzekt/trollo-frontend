import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../security/authentication.service";
import {TokenStorage} from "../../security/token-storage.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  returnUrl: string;
  userId: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private token: TokenStorage,
    private toaster: ToastrService
  ) {
  }

  ngOnInit() {
    this.userId = sessionStorage.getItem('userId');
    localStorage.clear();
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    sessionStorage.setItem('currentUserEmail', this.f.email.value);
    this.authenticationService
      .loginClient(this.f.email.value, this.f.password.value)
      .toPromise()
      .then(data => {
        this.token.saveToken(data.token);
        sessionStorage.setItem('userId', data._id.toString());
        this.router.navigate([this.returnUrl]);
      })
      .catch(e => {
        this.loading = false;
        console.log("error");
        this.toaster.error('bad credentials');
      });
  }

  get f() {
    return this.loginForm.controls;
  }

  getErrorMessage() {
    return this.f.email.hasError('required') ? 'Wprowadź wartość' : this.f.email.hasError('email') ? 'Wprowadź poprawny adres email' : '';
  }
}


