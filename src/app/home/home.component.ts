import {Component, OnInit} from '@angular/core';
import {BoardService} from '../services/board.service';
import {MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {AuthenticationService} from "../security/authentication.service";

interface BoardQuery {
  boardName: string;
  creatorID: string;
  id: string;
  subscriberID: string;
}

class BoardAddRequest {
  boardName: string;
  creatorID: {_id: string};
  subscriberID: {_id: string};
}

class BoardEditCommand {
  _id : string;
  boardName: string;
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  title = 'trello';
  private behavioralSubjectBoards$: BehaviorSubject<BoardQuery[]>;
  private observableBoards$: Observable<BoardQuery[]>;
  private boards: BoardQuery[];
  private userId: string;

  constructor(private booardService: BoardService,
              private toastr: ToastrService,
              private authenticationService: AuthenticationService,
              public dialog: MatDialog) {}

  ngOnInit(): void {

    this.behavioralSubjectBoards$ = new BehaviorSubject<BoardQuery[]>(null);
    this.observableBoards$ = this.behavioralSubjectBoards$.asObservable();
    this.userId = sessionStorage.getItem('userId');

    this.booardService.getBoards(this.userId)
      .subscribe(value => {
        this.boards = value;
        this.behavioralSubjectBoards$.next(this.boards);
      })
  }

  openDialog() {
    const dialogRef = this.dialog.open(AddBoardDialog);

    dialogRef.afterClosed().subscribe(result => {
      const boardRequest = new BoardAddRequest();
      boardRequest.boardName = result;
      boardRequest.creatorID = {"_id": this.userId};


      this.booardService.addBoard(boardRequest)
        .toPromise()
        .then(value => {
          this.booardService.getBoards(this.userId)
            .subscribe(value => {
              this.boards = value;
              this.behavioralSubjectBoards$.next(this.boards);

              this.toastr.success('Pomyslnie dodano tablice');
            })
        })
        .catch(reason => this.toastr.error('Upps nie udało się dodać tablicy'))
    });
  }

  edit(board: BoardQuery) {
    const dialogRef = this.dialog.open(EditBoardDialog);

    dialogRef.afterClosed().subscribe(result => {
      const newestBoard = new BoardEditCommand();
      newestBoard._id = board.id;
      newestBoard.boardName = result;

      this.booardService.editBoard(newestBoard)
        .toPromise()
        .then(value => {
          this.booardService.getBoards(this.userId)
            .subscribe(value => {
              this.boards = value;
              this.behavioralSubjectBoards$.next(this.boards);
            })
        })
        .catch(reason => this.toastr.error('ups, nie udalo sie zedytować tablicy') );
    });
  }

  logout() {
    sessionStorage.clear();
    location.reload();
  }


}


@Component({
  selector: 'dialog-add-board',
  templateUrl: 'dialog-add-board.html',
})
export class AddBoardDialog {

  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddBoardDialog>,
              private fb: FormBuilder) {
    this.form = this.fb.group({
      board: ''
    });
  }

  save() {
    this.dialogRef.close(this.form.value.board);
  }

  close() {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'dialog-edit-board',
  templateUrl: 'dialog-edit-board.html',
})
export class EditBoardDialog {

  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddBoardDialog>,
              private fb: FormBuilder) {
    this.form = this.fb.group({
      board: ''
    });
  }

  save() {
    this.dialogRef.close(this.form.value.board);
  }

  close() {
    this.dialogRef.close();
  }
}
