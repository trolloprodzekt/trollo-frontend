import { Injectable } from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent} from '@angular/common/http';
import {AuthenticationService} from "./authentication.service";
import {Observable} from "rxjs";
import {TokenStorage} from "./token-storage.service";


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService, private token: TokenStorage) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    if (this.token.getToken() != null) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.token.getToken()}`
        }
      });
    }

    return next.handle(request);
  }

}
