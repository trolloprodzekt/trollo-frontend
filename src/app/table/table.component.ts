import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject, forkJoin, Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup} from "@angular/forms";
import {TableService} from "./table.service";
import {UploadService} from "../services/upload.service";
import {Comment } from '../models/comment';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {DomSanitizer} from "@angular/platform-browser";

export class Activity {
   action: string;
   when: string;
}

export class AvailableFile {
  fileUrl: any;
  fileName: string;
  fileContentType: string;
  _id: string;
}

export class Table {
  _id: string;
  tableName: string;
  boardID: string;
  tableItems: [];
  constructor(){
    this.tableItems = [];
  }
}

export class Label {
  _id: string;
  labelTitle: string;
  labelColor: string;
  constructor(){  
   
  }
}

class TableItem {
  _id: string;
  itemTitle: string;
  description: string;
  tableID: string;
  activities: string;
  labelTitle: string;
  labelColor: string;
  labels:[];
  constructor(){
    this.labels=[];
    
  }
}

@Component({
  // selector: 'table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit{
  private behavioralSubjectTables$: BehaviorSubject<Table[]>;
  private observableTables$: Observable<Table[]>;
  private tables: any = [ ];
  private boardId: any;
  constructor(private route: ActivatedRoute,
              private toastr: ToastrService,
              private tableService: TableService,
              public dialog: MatDialog) {}

  ngOnInit(): void {
    this.tables = new Table();
    this.behavioralSubjectTables$ = new BehaviorSubject<Table[]>(null);
    this.observableTables$ = this.behavioralSubjectTables$.asObservable();
    this.boardId = this.route.snapshot.paramMap.get('id');
    this.getTables();

  }

  getTables(){
    this.tableService.getTablesByBoard(this.boardId)
      .subscribe(value => {
        this.tables = value;
        console.log(this.tables)
        this.behavioralSubjectTables$.next(this.tables);
      });
  }

  openDialog() {
    const dialogRef = this.dialog.open(AddTableDialog);

    dialogRef.afterClosed().subscribe(result => {
        console.log(result)
        if(result) {
          const table = new Table();
          table.tableName = result;
          table.boardID = this.boardId;

          this.tableService.addTable(table)
            .toPromise()
            .then(value => {
              this.tableService.getTablesByBoard(this.boardId)
                .subscribe(value => {
                  this.tables = value;
                  this.behavioralSubjectTables$.next(this.tables);

                  this.toastr.success('Pomyslnie dodano liste');
                })
            })
            .catch(reason => this.toastr.error('uups, cos poszlo nie tak'))
        }
    });
  }

  openDialogItemTable(id) {
    const dialogRef = this.dialog.open(AddTableItemDialog);

    dialogRef.afterClosed().subscribe(result => {
        if(result) {
          const tableItem = new TableItem();
          tableItem.itemTitle = result.value.itemTitle;
          tableItem.description = result.value.description;
          // tableItem.labelColor='warn';
          // tableItem.labelTitle='frontend';
          tableItem.tableID = id;
          console.log(tableItem);
          this.tableService.addTableItem(tableItem)
            .toPromise()
            .then(value => {
              this.tableService.getTablesByBoard(this.boardId)
                .subscribe(value => {
                  this.tables = value;
                 
                  this.behavioralSubjectTables$.next(this.tables);
                  this.toastr.success('Pomyslnie dodano liste');
                })
            })
            .catch(reason => this.toastr.error('uups, cos poszlo nie tak'))
        }
    });
  }

  logout() {
    sessionStorage.clear();
    location.reload();
  }

  openDialogCardInfo(card) {
    const dialogRef = this.dialog.open(TableItemInfoDialog, {
      width: '650px',
      height: '600px',
      data : card
    });
  }

  dropList(index, event: CdkDragDrop<string[]>) {

    if (event.previousContainer === event.container) {
     
      let p = event.previousIndex;
      let c = event.currentIndex;

      const temp = this.tables[index].tableItems[p];
      this.tables[index].tableItems[p] = this.tables[index].tableItems[c];
      this.tables[index].tableItems[c] = temp;

      console.log(this.tables[index]._id);
      this.tableService
      .updateTable(this.tables[index]._id, this.tables[index])
      .subscribe();
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);

      this.getTables();
    


    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }


  dropTable( event: CdkDragDrop<string[]>) {

    //todo not working
    console.log(event.previousIndex)
    console.log(event.currentIndex)
    let p = event.previousIndex;
    let c = event.currentIndex;

    const temp = this.tables[p];
    this.tables[p] = this.tables[c];
    this.tables[c] = temp;
    console.log(this.tables);

    this.tableService
      .updateBoard(this.boardId, this.tables)
      .subscribe(()=>this.getTables());

    


    if (event.previousContainer === event.container) {
    moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }
}


@Component({
  selector: 'dialog-add-table',
  templateUrl: 'dialog-add-table.html',
})
export class AddTableDialog {

  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddTableDialog>,
              private fb: FormBuilder) {
    this.form = this.fb.group({
      table: ''
    });
  }

  save() {
    this.dialogRef.close(this.form.value.table);
  }

  close() {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'dialog-add-table-item',
  templateUrl: 'dialog-add-table-item.html',
})
export class AddTableItemDialog {

  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddTableItemDialog>,
              private fb: FormBuilder) {
    this.form = this.fb.group({
      itemTitle : '',
      description: ''
    });
  }

  save() {
    this.dialogRef.close(this.form);
  }

  close() {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'dialog-add-label',
  templateUrl: 'dialog-add-label.html',
})
export class AddLabelDialog {

  form: FormGroup;
  colorsList: string[];

  selected: string;

  constructor(public dialogRef: MatDialogRef<AddLabelDialog>,
              private fb: FormBuilder) {
    this.form = this.fb.group({
      title : '',
      selected: ''
    });
    this.colorsList = ['warn', 'primary', 'accent'];
  }

  onClick(color){
   // console.log(color)
    this.form.controls.selected.setValue(color);

  }

  save() {
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close();
  }
}


function getFiles() {
  this.uploadService.getAllAttachments()
    .toPromise()
    .then(data => {

      for (var file of data) {
        let availableFile = new AvailableFile();
        availableFile.fileName = file.name;
        const blob = new Blob([file.files.data], {type: 'application/octet-stream'})
        availableFile.fileUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob))
        availableFile._id = file._id;
        this.availableFiles.push(availableFile);
      }

      this.behavioralSubjectFiles$.next(this.availableFiles);
    }).catch(reason => console.log(reason));
}

@Component({
  selector: 'table-item-info',
  templateUrl: 'table-item-info.html',
})
export class TableItemInfoDialog implements OnInit {

  progress: any;
  canBeClosed = true;
  primaryButtonText = 'Upload';
  showCancelButton = true;
  uploading = false;
  uploadSuccessful = false;
  comment: Comment;
  form: FormGroup;
  comments: Comment [];
  itemID: string;
  availableFiles: AvailableFile[] = [];
  private behavioralSubjectFiles$: BehaviorSubject<AvailableFile[]>;
  private observableFiles$: Observable<AvailableFile[]>;
  _activities: Activity[];

  constructor(public dialogRef: MatDialogRef<AddTableItemDialog>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public uploadService: UploadService,
              private service: TableService,
              private toaster: ToastrService,
              private domSanitizer: DomSanitizer,
              public dialog: MatDialog,
              private fb: FormBuilder) {

                this.behavioralSubjectFiles$ = new BehaviorSubject<AvailableFile[]>(null);
                this.observableFiles$ = this.behavioralSubjectFiles$.asObservable();

                this._activities = data.activities;

                this.itemID=data._id;
                this.form = this.fb.group({
                  userInput: ''
                });

                this.service.getCommentByTableItem(this.itemID)
                .subscribe(e=>{
                  this.comments = e;
                });
  }

  ngOnInit(): void {
    getFiles.call(this);
  }

  getColor(){
    let color = '#ba1488';
    return color;
  }

  openLabelDialog(tableItem) {
    const dialogRef = this.dialog.open(AddLabelDialog);

    dialogRef.afterClosed().subscribe(result => {
        console.log(result);
        if(result) {
          console.log(result)

          const tableItem = new TableItem();
          tableItem.itemTitle =this.data.itemTitle;
          tableItem.description = this.data.description;
          tableItem.labelColor = result.selected;
          tableItem.labelTitle = result.title;
          tableItem.tableID = this.data.tableID;
          console.log("updating.....");
          console.log(tableItem);

          this.service.updateTableItem(this.data._id,tableItem).subscribe();
    }
  }
    );
  }



  closeDialog() {
    console.log(this.files);
    if (this.uploadSuccessful) {
      return this.dialogRef.close();
    }
    this.uploading = true;
    this.progress = this.uploadService.upload(this.files);
    let allProgressObservables = [];
    for (let key in this.progress) {
      allProgressObservables.push(this.progress[key].progress);
    }

    this.primaryButtonText = 'Finish';
    this.canBeClosed = false;
    this.dialogRef.disableClose = true;
    this.showCancelButton = false;

    forkJoin(allProgressObservables).subscribe(end => {
      this.canBeClosed = true;
      this.dialogRef.disableClose = false;
      this.uploadSuccessful = true;
      this.uploading = false;
    });
  }

  onFilesAdded() {
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (let key in files) {
      if (!isNaN(parseInt(key))) {
        this.files.add(files[key]);
      }
    }
  }

  addComment(id){
    this.itemID = id;
    this.comment = new Comment();
    this.comment.commentTxt = this.form.value.userInput;
    this.comment.authorID = sessionStorage.getItem('userId');
    this.comment.tableItemID = id;
    console.log(this.comment)
    this.form.value.userInput=' ';
    this.service.addCommentToTableItem(this.comment)
    .subscribe();

    this.service.getCommentByTableItem(this.itemID)
    .subscribe(data=>{
      this.comments = data;
      console.log(this.comments);

    });
  }

  // @ts-ignore
  @ViewChild('file') file
  public files: Set<File> = new Set();

  addFiles() {
    this.file.nativeElement.click();
  }

  delete(_id: string) {
    this.uploadService.deleteAttachment(_id)
      .toPromise()
      .then(() => getFiles())
      .catch(() => getFiles())
  }
}
