import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {User} from "../../models/user";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;
  email = new FormControl('', [Validators.required, Validators.email]);

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private toaster: ToastrService
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')]],
      firstName: ['', Validators.maxLength(50)],
      lastName: ['', Validators.maxLength(50)],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  onSubmit(){
    this.submitted = true;
    if (this.registerForm.invalid) {
      this.router.navigate(['/register']);
      this.toaster.error('form is invalid');
      console.log("Form is invalid");
      return;
    }
    if (this.registerForm.value.password !== this.registerForm.value.confirmPassword) {
      console.log("Passwords are different");
      this.toaster.error('passwords are different');
      this.router.navigate(['/register']);
      return;
    }

    const userToRegister : User = new User();

    userToRegister.username = this.registerForm.value.email;
    userToRegister.firstName = this.registerForm.value.firstName;
    userToRegister.lastName = this.registerForm.value.lastName;
    userToRegister.password = this.registerForm.value.password;

    console.log(userToRegister);
    this.loading = true;
    this.userService.register(userToRegister)
      .toPromise()
      .then(()=> {
        console.log("success");
        this.toaster.info('user registred');
      })
      .catch(()=>{
        this.toaster.error('email is alreday in use');
        this.router.navigate(['/register']);
        this.loading = false;
      });
  }

  get f() {
    return this.registerForm.controls;
  }

  getErrorMessage() {
    return this.f.email.hasError('required') ? 'Wprowadź wartość' : this.f.email.hasError('email') ? 'Wprowadź poprawny adres email' : '';
  }
}
