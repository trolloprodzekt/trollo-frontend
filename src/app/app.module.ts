import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import {AddBoardDialog, HomeComponent, EditBoardDialog} from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatFormFieldModule, MatIconModule, MatInputModule, MatListModule,
  MatMenuModule, MatProgressBarModule, MatProgressSpinnerModule,
  MatToolbarModule,
  MatChipsModule,
  MatSelectModule
} from '@angular/material';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {ToastrModule} from 'ngx-toastr';
import {AddTableDialog, AddTableItemDialog, TableComponent, TableItemInfoDialog, AddLabelDialog} from './table/table.component';
import { RegisterComponent } from './registry/register/register.component';
import { LoginComponent } from './login/login/login.component';
import {TokenStorage} from "./security/token-storage.service";
import {JwtInterceptor} from "./security/jwt-interceptor.service";
import { CommentComponent } from './comment/comment.component';

@NgModule({
  declarations: [
    AppComponent,
    AddBoardDialog,
    AddLabelDialog,
    HomeComponent,
    EditBoardDialog,
    TableComponent,
    AddTableItemDialog,
    AddTableDialog,
    RegisterComponent,
    LoginComponent,
    TableItemInfoDialog,
    CommentComponent
  ],
  imports: [
    BrowserModule,
    ToastrModule.forRoot(),
    AppRoutingModule,
    MatMenuModule,
    MatProgressBarModule,
    MatIconModule,
    MatSelectModule,
    MatListModule,
    MatInputModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatChipsModule,
    DragDropModule
  ],
  entryComponents: [AddBoardDialog, EditBoardDialog, AddTableDialog, AddTableItemDialog, TableItemInfoDialog, AddLabelDialog],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    TokenStorage
  ],
  bootstrap: [AppComponent]
})


export class AppModule { }
