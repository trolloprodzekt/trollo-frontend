import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BoardService {

  constructor(private http: HttpClient) { }

  getBoards(id): Observable<any>{
    return this.http.get<any>('http://localhost:4000/boards/findByUserID?id='+id);
  }

  addBoard(body: any): Observable<any> {
    return this.http.post('http://localhost:4000/boards/add', body);
  }

  editBoard(board: any): Observable<any> {
    return this.http.put('http://localhost:4000/boards/update', board);
  }
}
