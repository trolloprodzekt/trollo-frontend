import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { Comment } from '../models/comment';
import { Table } from '../table/table.component';

@Injectable({
  providedIn: 'root'
})
export class TableService {
  constructor(private http: HttpClient) { }

  addTable(table: any): Observable<any> {
    return this.http.post(`http://localhost:4000/tables/add`, table);
  }

  getTablesByBoard(boardId: any ){
    return this.http.get<Table[]>(`http://localhost:4000/tables/getByBoardId?boardID=`+ boardId);
  }

  addTableItem(tableItem): Observable<any> {
    return this.http.post('http://localhost:4000/tableItems/add', tableItem);
  }

  addCommentToTableItem(comment){
    return this.http.post('http://localhost:4000/comments/add', comment);
  }

  getCommentByTableItem(id){
    return this.http.get<Comment[]>('http://localhost:4000/comments/getByTableItemId?id=' + id);
  }
  updateTable(tableid,body){
    return this.http.put(`http://localhost:4000/tables/update?id=` + tableid, body);
  }
  getTableItemsBYTableID(id){
    return this.http.get<[]>(`http://localhost:4000/tableItems/getByTableId?tableID=`+ id);
  }
  updateBoard(boardID,body){
    return this.http.put(`http://localhost:4000/boards/update?id=` + boardID, body);
  }

  updateTableItem(id,body){
    return this.http.put(`http://localhost:4000/tableItems/update?id=` + id, body);
  }
}
